//
//  ViewController.swift
//  TmsSample
//
//  Created by Jamia Purevdorj on 8/18/16.
//  Copyright © 2016 Mongol ID Group. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let overlay = MKTileOverlay(URLTemplate: "https://api.minu.mn/songo/{z}/{x}/{y}.png?token=iC2Xv8QX2D9OHErK2Ya56kxdsuQX1b7R")
        overlay.canReplaceMapContent = true
        
        mapView.addOverlay(overlay, level: .AboveLabels)
        mapView.showsUserLocation = true
        let region = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2D(latitude: 47.9188,longitude: 106.9177), 500, 500)
        mapView.setRegion(region, animated: true)
        mapView.showsCompass = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer! {
        
        if overlay.isKindOfClass(MKTileOverlay)
        {
            guard let tileOverlay = overlay as? MKTileOverlay else {
                return MKOverlayRenderer()
            }
            return MKTileOverlayRenderer(tileOverlay: tileOverlay)
        }
        
        return nil
    }


}

